import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
// This class runs the grep functions for Client.java 
public class RunClient{
	public static void main(String[] args)
	{
		int clientNum = 10;
		String host;
		int port;
		String cmdLine;
		int count= 0;
		List<Client> clientList = new List<Client>();
		for (int i = 0; i < clientNum; i++)
		{
			//TODO:Find host string here and the port number
			Client client = new Client(host,port);
			clientList.add(client);
		}

		//Infinitely cycle through all the clients
		//Performing grep operations on all of them
		while(true)
		{
			count = 0; 
			Scanner in = new Scanner(System.in);
			System.out.println("Enter a command");
			cmdLine = reader.nextLine();
			long startTime = System.currentTimeMillis();
			// Now go through the clients
			for(int i = 0; i < clientList.size(); i++)
			{
				Client currClient = clientList.get(i);
				count += currClient.clientGrep(cmd,i);
				System.out.println("  ");

			}
			long endTime = System.currentTimeMillis();
			System.out.println("Total lines:"+count);
			System.out.println("Total time:"+(endTime - startTime)+"in ms");
		}


	}
}