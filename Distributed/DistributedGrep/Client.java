import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client{
	//Client socket properties
	private String hostName;
	private int port;
	private Socket clientSocket;
	private DataOutputStream outputstream;
	private final grepError = -1;
	// Empty constructor
	public Client{}
	//Constructor
	public Client(String hostName, int port)
	{
		this.hostName = hostName;
		this.port = port;

	}
	/** This function resets all the Client class variables
	  */
	public void closeClient()
	{
		clientSocket = null;
		outputstream = null;
		hostName = " ";
	}

	/** This function starts a connection to the hostName and port.
	  * It creates a new socket and makes sure it is available
	  @returns true if succesful, false otherwise
    */
	  public boolean connect()
	  {
	  	try{
	  		clientSocket = new Socket(this.hostName,this.port);
	  		outputstream = new DataOutputStream(clientSocket.getOutputStream())
	  	}
	  	catch (Exception e)
	  	{
	  		// In case of exception, close the client and return false
	  		closeClient();
	  		return false;
	  	}

	  }

	  /** This function disconnects the client
	    * If the socket is not valid (connection not opened, it does nothing)
	    */
	    public void disconnect()
	    {
	    	try{
	    		clientSocket.close();
	    		outputstream.close();
	    	}
	    	catch(Exception e)
	    	{
	    		// Do nothing connection is closed
	    	}
	    	closeClient();

	    }

	    /** This function returns client availability
	      * If client is available, returns true, else 
	      * returns false
	      */
	    public boolean clientAvailable()
	    {
	    	if(clientSocket == null || outputstream == null)
	    		return false;
	    	return true;
	    }

	    /** This function does the client side grep
	      */
	      public int clientGrep(String cmd, int serverNum)
	      {
	      	//If the client is not ready, then open connection
	      	boolean connectStatus;
	      	if(!clientAvailable())
	      		connectStatus = connect();
	      	else
	      		connectStatus = true;
	      	if(connectStatus)
	      	{
	      		//Write grep command to the server
	      		outputstream.writeUTF(cmd);
	      		System.out.println("Server:"+serverNum+"Host Name:"+hostName);
	      		//Then read the grep results from the servers and output to System.out
	      		ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
	      		List<String> lines = (List<String>) inputStream.getObject();
	      		System.out.print("Number of lines:"+lines.size());
	      		return lines.size();
	      	}
	      	else
	      		return grepError;
	      }
}